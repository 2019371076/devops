// //Crear las variables de cache
// const CACHE_DYNAMIC = 'dynamic-v1' //Para los archivos que se van a descargar
// const CACHE_STATIC = 'static-v1' //App shell
// const CACHE_INMUTABLE = 'inmutable-v1'// CDN de terceros. LIBRERIAS

// const limpiarCache = (cacheName, numberItem) => {
//     caches.open(cacheName)
//         .then(cache => {
//             cache.keys()
//                 .then(keys => {
//                     if (keys.length > numberItem) {
//                         cache.delete(keys[0])
//                             .then(limpiarCache(cacheName, numberItem))
//                     }
//                 })
//         })

// }
// self.addEventListener('install', function (event) {

//     const cachePromise = caches.open(CACHE_STATIC).then(function (cache) {

//         return cache.addAll([
//             '/',
//             '/index.html',
//             '/js/app.js',
//             '/sw.js',
//             'static/js/bundle.js',
//             'favicon.ico',
//             'not-found.png'
//         ])
//     })
//     const cacheInmutable = caches.open(CACHE_INMUTABLE).then(function (cache) {

//         return cache.addAll([
//             'https://fonts.googleapis.com/css2?family=Roboto&display=swap'

//         ])
//     })
//     event.waitUntil(Promise.all([cachePromise, cacheInmutable]))
// })

// // Cache Network race
// self.addEventListener('fetch', function (event) {
//     //Construir la promesa que resulva ambas peticiones
//     const respuesta = new Promise((resolve, reject) => {
//         //
//         let peticionRechazada = false
//         const falloSolicitud = () => {
//             //Si la peticion es rechazada tanto en red coomo en cache
//             if (peticionRechazada) {
//                 if (/\.(png|jpeg)$/i.test(event.request.url)) {
//                     resolve(caches.match('/not-found.png'))
//                 } else {
//                     reject('No se encontro respuesta')
//                 }
//             } else {
//                 peticionRechazada = true
//             }
//         }
//         //Primero hace la solicitud a la red
//         fetch(event.request).then(res => {
//             res.ok ? resolve(res) : falloSolicitud()
//         }).catch(falloSolicitud)
//         //Luego busca en el cache
//         caches.match(event.request).then(res => {
//             res ? resolve(res) : falloSolicitud()
//         }).catch(falloSolicitud)

//     })
//     event.respondWith(respuesta)
// })

//Importa,os sw-utils
importScripts('js/sw-utils.js')

//Crear las variables de cache
const CACHE_DYNAMIC = 'dynamic-v6' //Para los archivos que se van a descargar
const CACHE_STATIC = 'static-v4' //App shell
const CACHE_INMUTABLE = 'inmutable-v1'// CDN de terceros. LIBRERIAS

const limpiarCache = (cacheName, numberItem) => {
    caches.open(cacheName)
        .then(cache => {
            cache.keys()
                .then(keys => {
                    if (keys.length > numberItem) {
                        cache.delete(keys[0])
                            .then(limpiarCache(cacheName, numberItem))
                    }
                })
        })

}

self.addEventListener('install', function (event) {

    const cachePromise = caches.open(CACHE_STATIC).then(function (cache) {

        return cache.addAll([

            '/',
            '/index.html',
            '/js/app.js',
            '/js/sw-utils.js',
            '/sw.js',
            'static/js/bundle.js',
            'not-found.png',
            '/pages/offline.html'

        ])
    })
    const cacheInmutable = caches.open(CACHE_INMUTABLE).then(function (cache) {

        return cache.addAll([

            'https://fonts.googleapis.com/css2?family=Roboto&display=swap'

        ])
    })
    event.waitUntil(Promise.all([cachePromise, cacheInmutable]))
})

//Eliminar cache anterior
self.addEventListener('activate', event => {
    const respuesta = caches.keys()
        .then(keys => {
            keys.forEach(key => {
                if (key !== CACHE_STATIC && key.includes('static')) {
                    return caches.delete(key)
                }
            })
        })
    event.respondWith(respuesta)
})

// self.addEventListener('fetch', function (event) {
//     //Cache with network fallback
//     const respuesta = caches.match(event.request)
//         .then(response => {
//             if (response) return response
//             //Si no existe el archivo lo descarga de la web
//             return fetch(event.request)
//                 .then(newResponse => {
//                     caches.open(CACHE_DYNAMIC)
//                         .then(cache => {
//                             cache.put(event.request, newResponse)
//                             limpiarCache(CACHE_DYNAMIC, 20)
//                         })
//                     return newResponse.clone()
//                 })//ToDo 2 Manejo de errores
//                 .catch(err => {
//                     if (event.request.headers.get('accept').includes('text/html')) {
//                         return caches.match('/pages/offline.html')
//                     }
//                 })
//         })
//     event.respondWith(respuesta)
// })

self.addEventListener('fetch', event => {
    const respuesta = caches.match(event.request)
        .then(res => {
            if (res) {
                return res
            } else {
                return fetch(event.request)
                    .then(newRes => {
                        return actualizaCacheDinamico(CACHE_DYNAMIC, event.request, newRes)
                    })
            }
        })
    event.respondWith(respuesta)
})