import logo from './logo.svg';
import './App.css';
import { useEffect } from 'react';
import { useState } from 'react';
import axios from 'axios';

function App() {
  const [notas, setNotas] = useState([])
  const [title, setTitle] = useState("")
  const [text, setText] = useState("")

  const getNotas = async () => {
    const resp = await axios.get('http://localhost:3001/api/note')
    console.log(resp.data)
    setNotas(resp.data)
  }

  useEffect(() => {
    getNotas()
  }, [])

  const agregarNota = async () => {
    console.log(title, text)
    const resp = await axios.post('http://localhost:3001/api/note', { title: title, text: text })
    console.log(resp)
  }

  return (
    <div className="App">
      <h1>Notas</h1>
      <div className="">
        <label htmlFor="title">Titulo</label>
        <input type="text" name="title" id="title" onChange={event => setTitle(event.target.value)} />
      </div>
      <div className="">
        <label htmlFor="text">Texto</label>
        <input type="text" name='text' id='text' onChange={event => setText(event.target.value)} />
      </div>
      <button onClick={agregarNota}>Agregar nota</button>
      <br />
      {notas.map((n, i) => (
        <div className="Nota" key={i}>
          <h5>{n.title}</h5>
          <p>{n.text}</p>
        </div>
      ))}
    </div>
  );
}

export default App;
